#!/usr/bin/bash

if [[ ! $(uname -i) =~ "aarch64" ]]; then
    echo "Only work on aarch64"
    exit 0
fi

if rpm -q --quiet opencsd; then
    :
else
    sudo dnf install -y opencsd
    if [[ $? != 0 ]]; then
        echo "install of opencsd failed!"
        exit 1
    fi
fi

if [[ ! -f /usr/lib64/libopencsd.so.1 ]]; then
    echo "/usr/lib64/libopencsd.so.1 not found!"
    exit 2
fi

cat << EOF > csd.c
#include <opencsd/c_api/opencsd_c_api.h>

/*
 * Check OpenCSD library version is sufficient to provide required features
 */
#define OCSD_MIN_VER ((1 << 16) | (2 << 8) | (1))
#if !defined(OCSD_VER_NUM) || (OCSD_VER_NUM < OCSD_MIN_VER)
#error "OpenCSD >= 1.2.1 is required"
#endif

int main(void)
{
	(void)ocsd_get_version();
	return 0;
}
EOF

gcc -Wall -Werror -lopencsd_c_api -lopencsd -o csd csd.c
if [[ $? -ne 0 ]]; then
    echo "Build against opencsd failed."
    exit 3
fi

./csd
if [[ $? -ne 0 ]]; then
    echo "OpenCSD runtime test failed."
    exit 4
fi

ldd ./csd | grep -q -i opencsd
if [[ $? -ne 0 ]]; then
    echo "test program does not link against opencsd, please investigate."
    exit 5
fi

exit 0
